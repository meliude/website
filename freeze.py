from flask_frozen import Freezer
from personalwebsite import app

app.config['FREEZER_BASE_URL'] = 'https://meliude.gitlab.io/website'
freezer = Freezer(app)

def product_url_generator():
    # URLs as strings
    yield '/website'
    yield '/website/home'
    yield '/website/research'
    #yield '/website/cv'

if __name__ == '__main__':
    freezer.freeze()
