from flask import Flask, render_template, url_for

app = Flask(__name__)

@app.route('/')
def ini():
    return render_template('home.html')

@app.route('/research')
def research():
    return render_template('research.html', title='Research')

#@app.route('/cv')
#def cv():
#    return render_template('cv.html', title='CV')

@app.route('/teaching')
def teaching():
    return render_template('teaching.html', title='Teaching')


if __name__ == '__main__':
    app.run(debug=True)
